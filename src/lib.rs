#[cfg(test)]
mod tests;

use std::collections::HashMap;

#[macro_export]
macro_rules! map {
    ($map_type: ident, $($key: expr => $val: expr),+) => {{
        let mut map = $map_type::new();
        $(
            map.insert($key, $val);
        )+
        map
    }};
    ($($key: expr => $val: expr),+) => {{
        map!(HashMap, $($key => $val),*)
    }};
}

#[macro_export]
macro_rules! multiple_max {
    ($x: expr) => ($x);
    ($x: expr, $($y: expr),+) => {
        {
            let a = multiple_max!($($y),+);
            if $x > a {
                $x
            } else {
                a
            }
        }
    }
}


#[macro_export]
macro_rules! multiple_min {
    ($x: expr) => ($x);
    ($x: expr, $($y: expr),+) => {
        {
            let a = multiple_min!($($y),+);
            if $x < a {
                $x
            } else {
                a
            }
        }
    }
}





