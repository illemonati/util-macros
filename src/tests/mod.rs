
use super::*;
use std::collections::BTreeMap;

#[test]
fn test_multiple_max() {
    assert_eq!(multiple_max!(-1, 2, 123, 12333, -928, 0), 12333);
}

#[test]
fn test_multiple_min() {
    assert_eq!(multiple_min!(-1, 2, 123, 12333, -928, 0), -928);
}

#[test]
fn test_map() {
    let mut test_hash_map = HashMap::new();
    test_hash_map.insert(1, 2);
    test_hash_map.insert(3, 4);
    test_hash_map.insert(-9, 14);
    let macro_hash_map = map!(1 => 2, 3 => 4, -9 => 14);
    let mut test_tree_map = BTreeMap::new();
    test_tree_map.insert(1, 2);
    test_tree_map.insert(3, 4);
    test_tree_map.insert(-9, 14);
    let macro_tree_map = map!(BTreeMap, 1 => 2, 3 => 4, -9 => 14);
    assert_eq!(test_hash_map, macro_hash_map);
    assert_eq!(test_tree_map, macro_tree_map);
}

